package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

import java.util.Scanner;

public class TestZad2 {

    public static void main(String[] args) {
	    Scanner in=new Scanner(System.in);
	    System.out.println("Podaj liczbę naturalną: ");
	    int n=in.nextInt();
	    int x;
	    int[] Tab=new int[n];
        for(int i=0; i<n; i++){
            System.out.println("Podaj "+(i+1)+" liczbę ciągu: ");
            x=in.nextInt();
            Tab[i]=x;
        }
        System.out.println("Odwrócony ciag: ");
	    for(int i=n-1; i>=0; i--){
	        System.out.print(Tab[i]+"\t");
        }

    }
}
