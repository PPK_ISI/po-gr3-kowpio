package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

public class Zad1_b {
    int b(int Tab[], int n) {
        int iloczyn=1;
        for(int i=0; i<n; i++){
            iloczyn *= Tab[i];
        }
        return iloczyn;
    }
}