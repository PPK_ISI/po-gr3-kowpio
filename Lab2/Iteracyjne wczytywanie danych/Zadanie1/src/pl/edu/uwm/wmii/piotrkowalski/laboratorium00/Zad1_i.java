package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

public class Zad1_i {
    double i(int Tab[], int n){
        double wynik=0;
        Silnia s=new Silnia();
        for(int i=1; i<=n; i++){
            wynik += (Math.pow((-1), i) * Tab[i-1])/s.silnia(i);
        }
        return wynik;
    }
}
