package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

public class Zad1_h {
    int h(int Tab[], int n){
        int wynik=0;
        for(int i=1; i<=n; i++){
            wynik += Math.pow((-1), i+1) * Tab[i-1];
        }
        return wynik;
    }
}
