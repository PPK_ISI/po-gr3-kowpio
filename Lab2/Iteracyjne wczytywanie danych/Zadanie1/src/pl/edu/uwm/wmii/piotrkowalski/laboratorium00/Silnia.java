package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

public class Silnia {
    public static int silnia(int x){
        if(x==0)
            return 1;
        else
            return x*silnia(x-1);
    }
}
