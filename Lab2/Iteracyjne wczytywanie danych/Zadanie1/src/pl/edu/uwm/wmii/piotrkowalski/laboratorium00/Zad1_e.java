package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

public class Zad1_e {
    int e(int Tab[], int n) {
        int iloczyn=1;
        for(int i=0; i<n; i++){
            iloczyn *= Math.abs(Tab[i]);
        }
        return iloczyn;
    }
}
