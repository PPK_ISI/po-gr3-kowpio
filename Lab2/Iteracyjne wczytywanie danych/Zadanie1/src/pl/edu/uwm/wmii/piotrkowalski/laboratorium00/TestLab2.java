package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

import java.util.Scanner;

public class TestZad1 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Podaj liczbę naturalną:");
        int n = in.nextInt();
        System.out.println(n);
        int Tab[] = new int[n];
        int x;
        for(int i=0; i<n; i++) {
            System.out.println("Podaj liczbę rzeczywistą nr " + (i + 1) + ":");
            x = in.nextInt();
            Tab[i] = x;
        }
        Zad1_a A = new Zad1_a();
        System.out.println("Wynik podpunktu a: "+A.a(Tab, n));
        Zad1_b B = new Zad1_b();
        System.out.println("Wynik podpunktu b: "+B.b(Tab, n));
        Zad1_c C = new Zad1_c();
        System.out.println("Wynik podpunktu c: "+C.c(Tab, n));
        Zad1_d D = new Zad1_d();
        System.out.println("Wynik podpunktu d: "+D.d(Tab, n));
        Zad1_e E = new Zad1_e();
        System.out.println("Wynik podpunktu e: "+E.e(Tab, n));
        Zad1_f F = new Zad1_f();
        System.out.println("Wynik podpunktu f: "+F.f(Tab, n));
        Zad1_g G = new Zad1_g();
        System.out.println("Wynik podpunktu g: "+G.g(Tab, n));
        Zad1_h H = new Zad1_h();
        System.out.println("Wynik podpunktu h: "+H.h(Tab, n));
        Zad1_i I = new Zad1_i();
        System.out.println("Wynik podpunktu i: "+I.i(Tab, n));

    }

}

