package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

public class Zad1_c {
    int c(int Tab[], int n) {
        int suma=0;
        for(int i=0; i<n; i++){
            suma += Math.abs(Tab[i]);
        }
        return suma;
    }
}