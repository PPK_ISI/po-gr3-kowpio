package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

public class Zad1_f {
    int f(int Tab[], int n) {
        int suma=0;
        for(int i=0; i<n; i++){
            suma += Math.pow(Tab[i], 2);
        }
        return suma;
    }
}
