package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

public class Zad1_g {
    String g(int Tab[], int n) {
        int suma=0;
        for(int i=0; i<n; i++){
            suma += (Tab[i]);
        }
        int iloczyn=1;
        for(int i=0; i<n; i++){
            iloczyn *= Tab[i];
        }
        String s = "iloczyn: " + iloczyn + "| " + "suma: " + suma;
        return s;
    }
}
