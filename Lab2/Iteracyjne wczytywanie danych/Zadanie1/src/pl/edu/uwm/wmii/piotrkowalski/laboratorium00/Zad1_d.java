package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

public class Zad1_d {
    double d(int Tab[], int n) {
        int suma=0;
        for(int i=0; i<n; i++){
            suma += Math.sqrt(Math.abs(Tab[i]));
        }
        return suma;
    }
}