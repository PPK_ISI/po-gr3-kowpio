package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

import java.util.Scanner;

public class TestZad1 {

    public static void main(String[] args) {
	    Scanner in=new Scanner(System.in);
	    System.out.println("Podaj liczbę naturalną: ");
	    int n=in.nextInt();
	    int Tab[]=new int[n];
	    int x;
	    for(int i=0; i<n; i++){
            System.out.println("Podaj "+(i+1)+" liczbę naturalną tablicy: ");
            x=in.nextInt();
            Tab[i]=x;
        }
	    zad1_a A=new zad1_a();
	    System.out.println("a) Ilość liczb nieparzystych w tablicy: "+A.np(Tab, n));
        zad1_b B=new zad1_b();
        System.out.println("b) Ilość liczb podzielnych przez 3\n   oraz niepodzielnych przez 5 w tablicy: "+B.p3(Tab, n));
        zad1_C C=new zad1_C();
        System.out.println("c) Ilość kwadratów liczby parzystej w tablicy: "+C.sqr_even(Tab, n));
        zad1_d D=new zad1_d();
        System.out.println("d) Ilość liczb spełniających warunek w tablicy: "+D.fun_d(Tab, n));
        zad1_e E=new zad1_e();
        System.out.println("e) Ilość liczb spełniających warunek w tablicy: "+E.fun_e(Tab, n));
        zad1_f F=new zad1_f();
        System.out.println("f) Ilość liczb spełniających warunek w tablicy: "+F.fun_f(Tab, n));
        zad1_g G=new zad1_g();
        System.out.println("g) Ilość liczb spełniających warunek w tablicy: "+G.fun_g(Tab, n));
        zad1_h H=new zad1_h();
        System.out.println("h) Ilość liczb spełniających warunek w tablicy: "+H.fun_h(Tab, n));
    }
}
