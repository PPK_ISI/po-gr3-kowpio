package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

public class zad1_e {
    int fun_e(int Tab[], int n){
        int licznik=0;
        Silnia s=new Silnia();
        for(int i=2; i<n-1; i++){
            if(Tab[i]>Math.pow(2, i) && Tab[i]<s.silnia(i))
                licznik++;
        }
        return licznik;
    }
}
