package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

import java.util.Scanner;

public class TestZad4 {

    public static void main(String[] args) {
        Scanner in=new Scanner(System.in);
        System.out.println("Podaj liczbę naturalną: ");
        int n=in.nextInt();
        double x;
        double max=0;
        double min=0;
        for(int i=0; i<n; i++) {
            System.out.println("Podaj " + (i + 1) + " liczbę rzeczywistą: ");
            x = in.nextDouble();
            if (x>max)
                max=x;
            if (x<min)
                min=x;
        }
        System.out.println("Największa z podanych liczb: "+max);
        System.out.println("Najmniejsza z podanych liczb: "+min);
    }
}

