package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

import java.util.Scanner;

public class TestZad5 {

    public static void main(String[] args) {
        Scanner in=new Scanner(System.in);
        System.out.println("Podaj liczbę naturalną: ");
        int n=in.nextInt();
        double x;
        int pary=0;
        double Tab[]=new double[n];
        for(int i=0; i<n; i++) {
            System.out.println("Podaj " + (i + 1) + " liczbę rzeczywistą tablicy: ");
            x = in.nextDouble();
            Tab[i]=x;
        }
        for(int i=0; i<n-1; i++) {
            if(Tab[i]>0 && Tab[i+1]>0)
                pary++;
        }
        System.out.println("Ilość par (a,b) spełniających warunek: "+pary);
    }
}
