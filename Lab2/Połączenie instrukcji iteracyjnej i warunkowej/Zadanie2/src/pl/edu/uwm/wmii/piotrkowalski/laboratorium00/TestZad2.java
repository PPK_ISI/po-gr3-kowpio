package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

import java.util.Scanner;

public class TestZad2 {

    public static void main(String[] args) {
        Scanner in=new Scanner(System.in);
        System.out.println("Podaj liczbę naturalną: ");
        int n=in.nextInt();
        double x;
        double suma=0;
        for(int i=0; i<n; i++){
            System.out.println("Podaj "+(i+1)+" liczbę rzeczywistą: ");
            x=in.nextDouble();
            if(x>0)
                suma+=x;
        }
        suma*=2;
        System.out.println("Podwojona suma liczb dodatnich: "+suma);
    }
}
