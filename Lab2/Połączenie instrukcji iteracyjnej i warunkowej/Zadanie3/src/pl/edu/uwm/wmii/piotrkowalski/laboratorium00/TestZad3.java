package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

import java.util.Scanner;

public class TestZad3 {

    public static void main(String[] args) {
        Scanner in=new Scanner(System.in);
        System.out.println("Podaj liczbę naturalną: ");
        int n=in.nextInt();
        double x;
        int dodatnie=0;
        int ujemne=0;
        int zera=0;
        for(int i=0; i<n; i++) {
            System.out.println("Podaj " + (i + 1) + " liczbę rzeczywistą: ");
            x = in.nextDouble();
            if (x==0)
                zera ++;
            else if (x>0)
                dodatnie++;
            else
                ujemne++;
        }
        System.out.println("Ilość liczb dodatnich: "+dodatnie);
        System.out.println("Ilość liczb ujemnych: "+ujemne);
        System.out.println("Ilość zer: "+zera);
    }
}
