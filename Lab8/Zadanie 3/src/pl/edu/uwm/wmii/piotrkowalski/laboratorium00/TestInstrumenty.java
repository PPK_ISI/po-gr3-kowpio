package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

import pl.imiajd.kowalski.Flet;
import pl.imiajd.kowalski.Fortepian;
import pl.imiajd.kowalski.Skrzypce;
import pl.imiajd.kowalski.Instrument;

import java.time.LocalDate;
import java.util.ArrayList;

public class TestInstrumenty {

    public static void main(String[] args) {
        ArrayList<Instrument> Orkiestra=new ArrayList<Instrument>();

        Orkiestra.add(new Flet("producent_fletow", LocalDate.parse("2005-04-29")));
        Orkiestra.add(new Flet("producent_fletow", LocalDate.parse("2007-05-15")));
        Orkiestra.add(new Fortepian("producent_fortepianow", LocalDate.parse("2012-04-29")));
        Orkiestra.add(new Skrzypce("producent_skrzypiec", LocalDate.parse("2008-02-29")));
        Orkiestra.add(new Skrzypce("producent_skrzypiec", LocalDate.parse("2008-04-19")));

        for(Instrument p:Orkiestra){
            System.out.println(p.dzwiek());
        }

        System.out.println("\nSkład orkiestry:");

        for(Instrument p:Orkiestra){
            System.out.println(p.toString());
        }
    }

}
