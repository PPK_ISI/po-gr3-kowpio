package pl.imiajd.kowalski;

import java.time.LocalDate;

public abstract class Instrument{
    private String producent;
    private LocalDate rokProdukcji;

    public Instrument(String producent, LocalDate rokProdukcji){
        this.producent = producent;
        this.rokProdukcji = rokProdukcji;
    }

    public String getProducent(){return producent;}
    public LocalDate getRokProdukcji(){return rokProdukcji;}
    public String toString(){return producent+" "+rokProdukcji;}
    public boolean equals(Instrument y){
        if(y.getProducent()==producent) return true;
        else return false;
    }
    public abstract String dzwiek();
}
