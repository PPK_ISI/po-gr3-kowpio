package pl.imiajd.kowalski;

import java.time.LocalDate;

public class Student extends Osoba
{
    public Student(String nazwisko, String[] imiona, LocalDate dataUrodzenia, boolean plec, String kierunek, double sredniaOcen)
    {
        super(nazwisko, imiona, dataUrodzenia, plec);
        this.kierunek = kierunek;
        this.sredniaOcen = sredniaOcen;
    }

    public double getSredniaOcen(){return sredniaOcen;}

    public String getOpis()
    {
        return "kierunek studiów: " + kierunek;
    }

    private double sredniaOcen;
    private String kierunek;
}
