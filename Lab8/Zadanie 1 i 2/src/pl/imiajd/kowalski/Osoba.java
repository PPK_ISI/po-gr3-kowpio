package pl.imiajd.kowalski;

import java.time.LocalDate;

public abstract class Osoba
{
    public Osoba(String nazwisko, String[] imiona, LocalDate dataUrodzenia, boolean plec)
    {
        this.nazwisko = nazwisko;
        this.imiona = imiona;
        this.dataUrodzenia = dataUrodzenia;
        this.plec = plec;
    }

    public abstract String getOpis();

    public String getNazwisko()
    {
        return nazwisko;
    }

    public String[] getImiona(){return imiona;}

    public LocalDate getDataUrodzenia(){return dataUrodzenia;}

    public String getPlec(){
        if (plec==true) return "kobieta";
        else return "mężczyzna";
    }

    private String[] imiona;
    private LocalDate dataUrodzenia;
    private boolean plec;
    private String nazwisko;
}
