package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

import pl.imiajd.kowalski.Osoba;
import pl.imiajd.kowalski.Pracownik;
import pl.imiajd.kowalski.Student;

import java.util.*;
import java.time.LocalDate;

public class TestOsoba
{
    public static void main(String[] args)
    {
        Osoba[] ludzie = new Osoba[2];

        String[] jan=new String[2];
        jan[0]="Jan"; jan[1]="";
        ludzie[0] = new Pracownik("Kowalski", 50000, jan, LocalDate.parse("1982-02-20"), false, LocalDate.parse("2000-02-20"));

        String[] malg=new String[2];
        malg[0]="Małgorzata"; malg[1]="";
        ludzie[1] = new Student("Nowak",malg , LocalDate.parse("1982-02-20"), true, "informatyka", 4.0);
        // ludzie[2] = new Osoba("Dyl Sowizdrzał");

        for (Osoba p : ludzie) {
            System.out.println(p.getNazwisko() + ": " + p.getOpis());
        }
    }
}
