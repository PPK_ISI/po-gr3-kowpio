package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

public class Zadanie9 {

    public static void main(String[] args) {
        String text= """
                (| |)     _________
                (O_o)    / Hello   '
                |   |   |    Junior |
                (")(")   ' Coder!  /
                          ---------

                """;
        text = text.replaceAll("'","\\\\");
        System.out.println(text);
    }
}
