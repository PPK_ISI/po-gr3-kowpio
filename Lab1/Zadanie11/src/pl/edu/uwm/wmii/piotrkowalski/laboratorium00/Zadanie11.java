package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

public class Zadanie11 {

    public static void main(String[] args) {
        System.out.println("""
                Dwadzieścia siedem kości,
                trzydzieści pięć mięśni,
                około dwóch tysięcy komórek nerwowych
                w każdej opuszce naszych pięciu palców.
                To zupełnie wystarczy,
                żeby napisać „Mein Kampf”
                albo „Chatkę Puchatka”.
                """);
    }
}
