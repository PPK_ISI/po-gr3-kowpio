package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

public class TestIntegerSet {

    public static void main(String[] args) {
        int[] tab1={2,67,13,5,7};
        int[] tab2={5,67,2,54,1};

        IntegerSet set1=new IntegerSet(tab1);
        IntegerSet set2=new IntegerSet(tab2);

        int[] tab3={};
        IntegerSet union=new IntegerSet(tab3);
        Boolean[] unionOutcome=union.union(set1.elementy, set2.elementy);
        union.elementy=unionOutcome;
        System.out.print("wynik sumy mnogości: ");
        union.getElements();
        System.out.println();

        int[] tab4={};
        IntegerSet intersection=new IntegerSet(tab4);
        Boolean[] intersectionOutcome=intersection.intersection(set1.elementy, set2.elementy);
        intersection.elementy=intersectionOutcome;
        System.out.print("wynik iloczynu mnogości : ");
        intersection.getElements();
        System.out.println();

        set1.insertElement(94);
        System.out.println();
        set2.deleteElement(54);
        System.out.println();

        System.out.println("wynik funkcji toString(): "+set1.toString());
        System.out.println();

        set1.equals(set2.elementy);
    }
}
