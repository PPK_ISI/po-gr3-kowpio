package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

public class IntegerSet {

    Boolean[] elementy=new Boolean[100];

    public IntegerSet(int tab[]){
        for(int i=0; i<tab.length; i++){
            elementy[tab[i]]=true;
        }
        for(int i=0; i<100; i++){
            if(elementy[i]==null) elementy[i]=false;
        }
    }

    public void getElements(){
        for(int i=0; i<100; i++){
            if(elementy[i]) System.out.print(i+" ");
        }
        System.out.println();
    }

    public static Boolean[] union(Boolean tab1[], Boolean tab2[]){
        Boolean[] wynik=new Boolean[100];
        for(int i=0; i<100; i++){
            if(tab1[i]==true || tab2[i]==true) wynik[i]=true;
            else wynik[i]=false;
        }
        return wynik;
    }

    public static Boolean[] intersection(Boolean tab1[], Boolean tab2[]){
        Boolean[] wynik=new Boolean[100];
        for(int i=0; i<100; i++){
            if(tab1[i]==true && tab2[i]==true) wynik[i]=true;
            else wynik[i]=false;
        }
        return wynik;
    }

    public void insertElement(int a){
        System.out.println("Elementy przed wstawieniem: ");
        this.getElements();
        this.elementy[a]=true;
        System.out.println("Elementy po wstawieniu: ");
        this.getElements();
    }
    public void deleteElement(int a){
        System.out.println("Elementy przed usunieciem: ");
        this.getElements();
        this.elementy[a]=false;
        System.out.println("Elementy po usunieciu: ");
        this.getElements();
    }

    public String toString(){
        String wynik="";
        for (int i=0;i<100; i++){
            if(elementy[i]==true) wynik+=(i+" ");
        }
        return wynik;
    }

    public void equals(Boolean set2[]){
        Boolean compare=true;
        for(int i=0; i<100; i++){
            if(elementy[i]!=set2[i]) compare=false;
        }
        if(compare==true) System.out.println("Zbiory sa rowne");
        else System.out.println("Zbiory nie sa rowne");
    }
}
