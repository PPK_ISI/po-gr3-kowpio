package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

public class RachunekBankowy {

    static double stopaProcentowa=0.1;
    private double saldo;

    public RachunekBankowy(double saldo){
        this.saldo=saldo;
    }

    public void obliczMiesieczneOdsetki(){
        double odsetki=(saldo*stopaProcentowa)/12;
        this.saldo+=odsetki;
    }

    public void setRocznaStopaProcentowa(double a){
        this.stopaProcentowa=a;
    }

    public void getSaldo(){
        System.out.format("%.2f%n", saldo);
        System.out.println();
    }

}
