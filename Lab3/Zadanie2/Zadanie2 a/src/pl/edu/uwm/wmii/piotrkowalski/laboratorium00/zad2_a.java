package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

import java.util.Random;

public class zad2_a {

    public static class Typ{
        int parz(int tab[], int n){
            int p=0;
            for(int i=0; i<n; i++){
                if (tab[i]%2==0)
                    p++;
            }
            return p;
        }
        int nparz(int tab[], int n){
            int np=0;
            for(int i=0; i<n; i++){
                if (tab[i]%2!=0)
                    np++;
            }
            return np;
        }
    }

    public static void main(String[] args) {
        int n=12;
        Random r=new Random();
        int tab[]=new int[n];
        for(int i=0; i<n; i++){
            tab[i]=r.nextInt(1999)-999;
            System.out.print(tab[i]+"\t");
        }
        System.out.println();
        Typ p = new Typ();
        Typ np = new Typ();
        System.out.println("Ilość elementów parzystych w tablicy: " + p.parz(tab, n));
        System.out.println("Ilość elementów nieparzystych w tablicy: " + p.nparz(tab, n));
    }

}
