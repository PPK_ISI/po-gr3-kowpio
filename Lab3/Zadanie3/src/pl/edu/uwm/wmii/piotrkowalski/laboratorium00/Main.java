package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

import java.util.Scanner;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
        int m=6, n=5, k=4;
        int macierz1[][]=new int[m][n];
        int macierz2[][]=new int[n][k];
        Random rand = new Random();
        //pierwsza macierz:
        for(int i=0; i<m; i++)
            for(int j=0; j<n; j++)
                macierz1[i][j] = rand.nextInt(4)+3;
        //druga macierz:
        for(int i=0; i<n; i++)
            for(int j=0; j<k; j++)
                macierz2[i][j] = rand.nextInt(4)+3;
        //wyzerowanie macierzy wynikowej
        int wynik[][]=new int[m][k];
        for(int i=0; i<n; i++)
            for(int j=0; j<k; j++)
                wynik[i][j] = 0;
        //wypisanie macierzy 
        System.out.println("Macierz A");
        for(int i=0; i<m; i++) {
            for (int j = 0; j < n; j++)
                System.out.print(macierz1[i][j] + " ");
            System.out.println();
        }
        System.out.println(); System.out.println("Macierz B");
        for(int i=0; i<n; i++) {
            for (int j = 0; j < k; j++)
                System.out.print(macierz2[i][j] + " ");
            System.out.println();
        }
        int wiersz=0;
        int kolumna=0;
        int suma=0;
        for(int i=0; i<m; i++) {
            for (int j = 0; j < k; j++) {
                while (wiersz <= m && kolumna <= k) {
                    suma += macierz1[i][kolumna] * macierz2[wiersz][j];
                    wiersz++;
                    kolumna++;
                }
                wynik[i][j] = suma;
                suma = 0;
                wiersz = 0;
                kolumna = 0;
            }
        }
        //wypisanie macierzy wynikowej C = A * B
        System.out.println(); System.out.println("Iloczyn macierzy A * B");
        for(int i=0; i<m; i++) {
            for (int j = 0; j < k; j++)
                System.out.print(wynik[i][j] + " ");
            System.out.println();
        }
    }
}
