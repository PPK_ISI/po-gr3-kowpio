package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

import java.util.Random;

public class Main {

    public static void main(String[] args) {
        int n=100;
        Random r=new Random();
        int tab[]=new int[n];
        int sum_d=0;
        int sum_u=0;
        for(int i=0; i<n; i++){
            tab[i]=r.nextInt(1999)-999;
            if (tab[i]>0)
                sum_d+=tab[i];
            else if(tab[i]<0)
                sum_u+=tab[i];
        }
        System.out.println("Suma dodatnich: "+sum_d+"\nSuma ujemnych: "+sum_u);

    }
}