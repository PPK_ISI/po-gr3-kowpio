package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

import java.util.Random;

public class Main {

    public static void main(String[] args) {
        int n=15;
        Random r=new Random();
        int tab[]=new int[n];
        int licznik=0;
        int maks=0;
        for(int i=0; i<n; i++){
            tab[i]=r.nextInt(1999)-999;
            if (tab[i]>0) {
                licznik++;
                if(licznik>maks)
                    maks=licznik;
            }
            else if(tab[i]<0)
                licznik=0;
        }
        for(int i=0; i<n; i++)
            System.out.print(tab[i]+" ");
        System.out.println("");
        System.out.println("Najdłuższy ciąg liczb dodatnich: "+maks);

    }
}