package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

import java.util.Random;

public class Main {

    public static void main(String[] args) {
        int n=100;
        Random r=new Random();
        int tab[]=new int[n];
        int maks=0;
        int il=0;
        for(int i=0; i<n; i++){
            tab[i]=r.nextInt(1999)-999;
            if (tab[i]>maks)
                maks=tab[i];

        }
        for(int i=0; i<n; i++)
            if(tab[i]==maks)
                il++;
        System.out.println("Najwi. liczba: "+maks+"\nWystąpień: "+il);

    }
}