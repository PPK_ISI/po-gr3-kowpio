package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

import java.util.Random;

public class Main {

    public static void main(String[] args) {
        int n=100;
        Random r=new Random();
        int tab[]=new int[n];
        int p=0;
        int np=0;
        for(int i=0; i<n; i++){
            tab[i]=r.nextInt(1999)-999;
            if (tab[i]%2==0)
                p++;
            else
                np++;
        }
        System.out.println("Parzystych: "+np);
        System.out.println("Nieparzystych: "+p);
    }
}
