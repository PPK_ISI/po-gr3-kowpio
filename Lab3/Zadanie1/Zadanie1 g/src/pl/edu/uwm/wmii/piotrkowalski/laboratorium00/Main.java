package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

import java.util.Random;

public class Main {

    public static void main(String[] args) {
        int n=11;
        int lewy=3;
        int prawy=7;
        int pojemnik;
        int licznik=1;
        Random r=new Random();
        int tab[]=new int[n];
        for(int i=0; i<n; i++){
            tab[i]=r.nextInt(1999)-999;
        }
        for(int i=0; i<n; i++)
            System.out.print(tab[i]+" ");
        System.out.println();
        for(int i=lewy-1; i<(lewy+prawy)/2; i++){
            pojemnik=tab[i];
            tab[i]=tab[prawy-licznik];
            tab[prawy-licznik]=pojemnik;
            licznik++;
        }
        for(int i=0; i<n; i++)
            System.out.print(tab[i]+" ");
    }
}