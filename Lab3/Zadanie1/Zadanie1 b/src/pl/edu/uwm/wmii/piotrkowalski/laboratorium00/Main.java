package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

import java.util.Random;

public class Main {

    public static void main(String[] args) {
        int n=100;
        Random r=new Random();
        int tab[]=new int[n];
        int d=0;
        int u=0;
        int z=0;
        for(int i=0; i<n; i++){
            tab[i]=r.nextInt(1999)-999;
            if (tab[i]==0)
                z++;
            else if(tab[i]>0)
                d++;
            else
                u++;
        }
        System.out.println("Dodatnich: "+d);
        System.out.println("Ujemnych: "+u);
        System.out.println("Zerh: "+z);
    }
}
