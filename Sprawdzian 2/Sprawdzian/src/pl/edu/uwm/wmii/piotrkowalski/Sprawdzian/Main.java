package pl.edu.uwm.wmii.piotrkowalski.Sprawdzian;

import pl.imiajd.kowalski.Komputer;
import pl.imiajd.kowalski.Laptop;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;

public class Main {

    public static void redukuj(LinkedList<String> komputery, int n){
        for(int i=n-1; i<komputery.size(); i=i+n)
            komputery.remove(i);
    }

    public static void main(String[] args) {
        ArrayList<Komputer> grupa = new ArrayList<>();

        grupa.add(new Komputer("IBM", LocalDate.parse("2001-12-01")));
        grupa.add(new Komputer("IBM", LocalDate.parse("2011-03-02")));
        grupa.add(new Komputer("Cooler Master", LocalDate.parse("2011-09-21")));
        grupa.add(new Komputer("Silentium PC", LocalDate.parse("2011-09-21")));
        grupa.add(new Komputer("ACER", LocalDate.parse("2020-07-11")));

        System.out.println("_______________________________________________");
        System.out.println("Nieposortowana lista komputerów:");
        System.out.println("_______________________________________________");

        for (Komputer e: grupa) {
            System.out.println("Nazwa: "+e.getNazwa()+" data produkcji: "+e.getDataProdukcji());
        }

        Collections.sort(grupa);

        System.out.println("_______________________________________________");
        System.out.println("Posortowana lista komputerów:");
        System.out.println("_______________________________________________");

        for (Komputer e: grupa) {
            System.out.println("Nazwa: "+e.getNazwa()+" data produkcji: "+e.getDataProdukcji());
        }

        LinkedList<String> new_grupa=new LinkedList<>();
        for (Komputer e: grupa) {
            new_grupa.add(e.toString());
        }

        redukuj(new_grupa, 2);

        System.out.println("_______________________________________________");
        System.out.println("Lista komputerów po zredukowaniu metodą redukuj():");
        System.out.println("_______________________________________________");

        for (String e: new_grupa) {
            System.out.println(e);
        }

        ArrayList<Laptop> grupaLaptopow = new ArrayList<>();

        grupaLaptopow.add(new Laptop("ASUS", LocalDate.parse("2001-12-01"), false));
        grupaLaptopow.add(new Laptop("ASUS", LocalDate.parse("2012-03-02"), false));
        grupaLaptopow.add(new Laptop("Macbook12", LocalDate.parse("2011-09-21"), true));
        grupaLaptopow.add(new Laptop("Macbook11", LocalDate.parse("2011-09-21"), true));
        grupaLaptopow.add(new Laptop("ACER", LocalDate.parse("2011-09-21"), false));

        System.out.println("_______________________________________________");
        System.out.println("Nieposortowana lista laptopów:");
        System.out.println("_______________________________________________");

        for (Laptop e: grupaLaptopow) {
            System.out.println("Nazwa: "+e.getNazwa()+" data produkcji: "+e.getDataProdukcji());
        }

        Collections.sort(grupa);

        System.out.println("_______________________________________________");
        System.out.println("Posortowana lista laptopów:");
        System.out.println("_______________________________________________");

        for (Laptop e: grupaLaptopow) {
            System.out.println("Nazwa: "+e.getNazwa()+" data produkcji: "+e.getDataProdukcji()+" Czy apple: "+e.getCzyApple());
        }

    }
}
