package pl.imiajd.kowalski;

import java.time.LocalDate;

public class Laptop extends Komputer implements Cloneable, Comparable<Komputer>{

    public Laptop(String nazwa, LocalDate dataProdukcji, boolean czyApple) {
        super(nazwa, dataProdukcji);
        this.czyApple = czyApple;
    }

    public boolean getCzyApple(){
        return czyApple;
    }

    public int compareTo(Laptop l) {
        if(nazwa.charAt(0) > l.getNazwa().charAt(0))
            if(dataProdukcji.isBefore(l.getDataProdukcji()))
                return 1;
        return -1;
    }

    public String toString(){
        return this.nazwa+" "+this.dataProdukcji+" "+this.czyApple;
    }

    private String nazwa;
    private LocalDate dataProdukcji;
    private boolean czyApple;
}
