package pl.imiajd.kowalski;

import java.time.LocalDate;

public class Komputer implements Cloneable, Comparable<Komputer>{

    public Komputer(String nazwa, LocalDate dataProdukcji){
        this.nazwa = nazwa;
        this.dataProdukcji = dataProdukcji;
    }

    public String getNazwa(){
        return nazwa;
    }

    public LocalDate getDataProdukcji(){
        return dataProdukcji;
    }

    public boolean equals(Komputer k){
        if(nazwa == k.getNazwa())
            return dataProdukcji == k.getDataProdukcji();
        return false;
    }

    public String toString(){
        return this.nazwa+" "+this.dataProdukcji;
    }

    @Override
    public int compareTo(Komputer k) {
        if(nazwa.charAt(0) > k.getNazwa().charAt(0))
            if(dataProdukcji.isBefore(k.getDataProdukcji()))
                return 1;
        return -1;
    }

    @Override
    public Komputer clone(){
        return this;
    }

    private String nazwa;
    private LocalDate dataProdukcji;
}
