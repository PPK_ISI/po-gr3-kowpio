package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

import java.time.LocalDate;

public class TestArrayUtil {

    public static void main(String[] args) {
        Integer[] tab={8, 2, 1, 9, 5};
        tab=ArrayUtil.selectionSort(tab);
        ArrayUtil.printArray(tab);
        System.out.println("\n");
        LocalDate[] d= {LocalDate.parse("2019-03-02"), LocalDate.parse("2049-02-02")
                , LocalDate.parse("2011-03-02"), LocalDate.parse("2019-09-01")
                , LocalDate.parse("2013-12-01")};
        d=ArrayUtil.selectionSort(d);
        ArrayUtil.printArray(d);
    }

    public static class ArrayUtil<T extends Comparable>{

        public static <T extends Comparable> void printArray(T[] tab){
            for (int i=0; i<tab.length; i++){
                System.out.print(tab[i]+",");
            }
        }

        public static <T extends Comparable> T[] selectionSort(T[] tab){
            int min_indeks;
            T min=tab[0];
            for(int i=0; i<tab.length-1; i++) {
                min_indeks=i;
                min=tab[i];
                for (int j = i + 1; j < tab.length; j++) {
                    if (min.compareTo(tab[j]) > 0) {
                        min = tab[j];
                        min_indeks = j;
                    }
                }
                tab[min_indeks]=tab[i];
                tab[i]=min;
                //Wypisanie poszczególnych etapów sortowania
                /*printArray(tab);
                System.out.println();*/
            }
            return tab;
        }
    }
}
