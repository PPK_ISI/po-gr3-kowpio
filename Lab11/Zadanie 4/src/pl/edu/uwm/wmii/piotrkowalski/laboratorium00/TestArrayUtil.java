package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

import java.time.LocalDate;

public class TestArrayUtil {

    public static void main(String[] args) {
        Integer[] t={1, 2, 3, 4, 5, 6, 7};
        System.out.println(ArrayUtil.binSearch(t, 5));
        System.out.println(ArrayUtil.binSearch(t, 10));

        System.out.println();

        LocalDate[] d={LocalDate.parse("2019-08-02"), LocalDate.parse("2012-08-12")
                        , LocalDate.parse("2016-02-16")};
        System.out.println(ArrayUtil.binSearch(d, LocalDate.parse("2016-02-16")));
        System.out.println(ArrayUtil.binSearch(d, LocalDate.parse("2011-02-16")));
    }

    public static class ArrayUtil<T extends Comparable>{

        public static <T extends Comparable> int binSearch(T[] tab, T wanted){
            int a=0;
            int b=tab.length-1;
            int current;
            while(a<=b){
                current=(int) Math.floor((a+b)/2);
                if (tab[current].compareTo(wanted)<0)
                    a=current+1;
                else if (tab[current].compareTo(wanted)>0)
                    b= current-1;
                else
                    return current;
            }
            return -1;
        }

    }
}
