package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

import java.time.LocalDate;
import java.util.Arrays;

public class TestArrayUtil {

    public static void main(String[] args) {
        Integer[] tab={8, 2, 1, 9, 5};
        tab=ArrayUtil.mergeSort(tab);
        ArrayUtil.printArray(tab);
        System.out.println("\n");
        LocalDate[] d= {LocalDate.parse("2019-03-02"), LocalDate.parse("2049-02-02")
                        , LocalDate.parse("2011-03-02"), LocalDate.parse("2019-09-01")
                        , LocalDate.parse("2013-12-01")};
        d=ArrayUtil.mergeSort(d);
        ArrayUtil.printArray(d);
    }

    public static class ArrayUtil<T extends Comparable>{
        
        public static <T extends Comparable> void printArray(T[] tab){
            for (int i=0; i<tab.length; i++){
                System.out.print(tab[i]+",");
            }
        }

        public static <T extends Comparable> T[] mergeSort(T[] tab){
            int a=0;
            int b=tab.length-1;
            if(tab.length>1) {
                int s = (a + b) / 2;
                T L[] = Arrays.copyOfRange(tab, a, s+1);
                T R[] = Arrays.copyOfRange(tab, s + 1, b+1);
                //Wypisanie poszczególnych etapów sortowania:
                /*
                printArray(L);
                System.out.print(" | ");
                printArray(R);
                System.out.println();
                */
                mergeSort(L);
                mergeSort(R);

                int i = 0;
                int j = 0;
                int k = 0;
                while (i < L.length && j < R.length) {
                    if (L[i].compareTo(R[j]) < 0) {
                        tab[k] = L[i];
                        i++;
                    } else {
                        tab[k] = R[j];
                        j++;
                    }
                    k++;
                }
                while (i < L.length) {
                    tab[k] = L[i];
                    k++;
                    i++;
                }
                while (j < R.length) {
                    tab[k] = R[j];
                    k++;
                    j++;
                }
            }
            return tab;
        }
    }

}
