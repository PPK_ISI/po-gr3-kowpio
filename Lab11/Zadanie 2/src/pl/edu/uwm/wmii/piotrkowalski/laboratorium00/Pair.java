package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

public class Pair<T> {

    public Pair() {
        first = null;
        second = null;
    }

    public Pair (T first, T second) {
        this.first = first;
        this.second = second;
    }

    public T getFirst() {
        return first;
    }
    public T getSecond() {
        return second;
    }

    public void setFirst (T newValue) {
        first = newValue;
    }
    public void setSecond (T newValue) {
        second = newValue;
    }

    public <T> void swap(){
        f_= this.first;
        this.first=this.second;
        this.second=f_;
    }
    private T f_=null;
    private T first;
    private T second;

}
