package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

public class PairUtil<T>{

    public static <T> Pair<T> swap(Pair<T> o){
        Pair<T> n= new Pair<T>();
        n.setFirst(o.getSecond());
        n.setSecond(o.getFirst());
        return n;
    }
}
