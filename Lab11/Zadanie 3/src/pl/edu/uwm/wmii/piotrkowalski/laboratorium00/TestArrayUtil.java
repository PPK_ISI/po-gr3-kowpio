package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

import java.util.ArrayList;
import java.time.LocalDate;

public class TestArrayUtil {

    public static void main(String[] args) {
        Integer[] n_sorted={1, 2, 3, 4, 5, 6};
        Integer[] n_not_sorted={6, 4, 3, 4, 5, 1};
        Integer[] n_sorted_desc={6, 5, 4, 3, 2, 1};
        System.out.println(ArrayUtil.isSorted(n_sorted));
        System.out.println(ArrayUtil.isSorted(n_not_sorted));
        System.out.println(ArrayUtil.isSorted(n_sorted_desc));

        System.out.println();
        
        LocalDate[] d_sorted={LocalDate.parse("2013-03-25"), LocalDate.parse("2014-01-22")
                , LocalDate.parse("2016-10-17")};
        LocalDate[] d_not_sorted={ LocalDate.parse("2014-01-22"), LocalDate.parse("2016-10-17")
                , LocalDate.parse("2013-03-25")};
        LocalDate[] d_sorted_desc={LocalDate.parse("2016-10-17"), LocalDate.parse("2014-01-22")
                , LocalDate.parse("2013-03-25")};
        System.out.println(ArrayUtil.isSorted(d_sorted));
        System.out.println(ArrayUtil.isSorted(d_not_sorted));
        System.out.println(ArrayUtil.isSorted(d_sorted_desc));
    }

    public static class ArrayUtil<T extends Comparable<T>>{

        public static <T extends Comparable<T>> boolean isSorted(T[] tab){
            for(int i=0; i<tab.length-1; i++) {
                if (tab[i].compareTo(tab[i + 1]) >0 ) return false;
            }
            return true;
        }
    }

}
