package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

import java.util.Scanner;

public class zad1b {

    public static int ilosc(String a, String b){
        int l=a.length();
        int l2=b.length();
        int licznik=0;
        String placeholder;
        for(int i=0; i<l-l2+1; i++){
            placeholder=String.valueOf(a.subSequence(i, i+l2));
            if (placeholder.compareTo(b)==0)
                licznik++;
        }
        return licznik;
    }

    public static void main(String[] args) {
        Scanner input=new Scanner(System.in);
        System.out.println("Podaj pierwszy napis: ");
        String napis1=input.nextLine();
        System.out.println("Podaj drugi napis: ");
        String napis2=input.nextLine();
        if(napis1.length() < napis2.length())
            System.out.println("Napis 2 powinien być krótszy lub równej długości jak napis 1.");
        else if(napis1.compareTo(napis2)==0)
            System.out.println("Napis 2 występuje w napisie 1 tylko raz.");
        else
            System.out.println("Napis 2 występuje w napisie "+ilosc(napis1, napis2)+" razy");
    }
}