package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

import java.util.Scanner;

public class zad1g {
    public static String nice(String a){
        StringBuffer wynik=new StringBuffer();
        int l=a.length();
        int licznik=1;
        for(int i=0; i<l; i++){
            if (licznik==3) {
                wynik.append(a.charAt(i));
                wynik.append("'");
                licznik=0;
            }
            else{
                wynik.append(a.charAt(i));
            }
            licznik++;
        }
        return wynik.toString();
    }

    public static void main(String[] args) {
        Scanner input=new Scanner(System.in);
        System.out.println("Podaj napis do modyfikacji: ");
        String napis=input.nextLine();
        System.out.println("Napis po modyfikacji: "+nice(napis));
    }
}
