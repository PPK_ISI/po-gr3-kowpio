package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

import java.util.Scanner;

public class Main {
    public static String change(String a){
        StringBuffer nowy=new StringBuffer(a);
        int l=a.length();
        char placeholder=' ';
        for(int i=0; i<l; i++){
            if(a.charAt(i)==a.toUpperCase().charAt(i)) {
                placeholder=a.toLowerCase().charAt(i);
                nowy.setCharAt(i, placeholder);
            }
            else{
                placeholder=a.toUpperCase().charAt(i);
                nowy.setCharAt(i, placeholder);
            }
        }
        return nowy.toString();
    }

    public static void main(String[] args) {
        Scanner input=new Scanner(System.in);
        System.out.println("Podaj napis do modyfikacji: ");
        String napis=input.nextLine();
        System.out.println("Napis po modyfikacji: "+change(napis));

    }
}
