package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

import java.util.Scanner;

public class zad1a {

    public static int ilosc(String a, char b){
        int l=a.length();
        int licznik=0;
        for(int i=0; i<l; i++)
            if(a.charAt(i)==b)
                licznik++;
        return licznik;
    }

    public static void main(String[] args) {
        Scanner input=new Scanner(System.in);
        System.out.println("Podaj napis: ");
        String napis=input.nextLine();
        System.out.println("Podaj znak: ");
        char x=input.next().charAt(0);
        System.out.println("Znak wystepuje w tekscie: "+ilosc(napis, x)+" razy.");
    }
}
