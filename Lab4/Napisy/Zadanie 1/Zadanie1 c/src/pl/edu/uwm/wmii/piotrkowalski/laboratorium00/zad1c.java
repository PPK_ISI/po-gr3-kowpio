package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

import java.util.Scanner;

public class zad1c {

    public static String middle(String a){
        int l=a.length();
        String wynik;

        double srodek=l/2;
        if(l%2==0){
            wynik=a.valueOf(a.subSequence((int)Math.floor(srodek)-1,(int)Math.floor(srodek)+1));
        }
        else
            wynik=a.valueOf(a.charAt((int)srodek));
        return wynik;
    }

    public static void main(String[] args) {
        Scanner input=new Scanner(System.in);
        System.out.println("Podaj pierwszy napis: ");
        String napis1=input.nextLine();
        System.out.println("Srodek napisu to "+middle(napis1));
    }
}
