package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Scanner;

public class zad5 {

    public static BigDecimal kap(int n, int k, double p){
        BigDecimal kapital=BigDecimal.valueOf(k);
        BigDecimal okres=BigDecimal.valueOf(n);
        BigDecimal stopa=BigDecimal.valueOf(p);
        BigDecimal placeholder=kapital.multiply(stopa).multiply(okres);
        BigDecimal wynik=kapital.add(kapital);
        return wynik;
    }

    public static void main(String[] args) {
        Scanner in=new Scanner(System.in);
        System.out.println("Podaj okres oszczędzania w latach: ");
        int n=in.nextInt();
        System.out.println("Podaj kapitał początkowy: ");
        int k=in.nextInt();
        System.out.println("Podaj stopę procentową: ");
        double p=in.nextDouble();
        System.out.println("Wielkość kapitału: "+ kap(n, k, p).toString());
    }
}
