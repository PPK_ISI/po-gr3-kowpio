package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

import java.math.BigInteger;
import java.util.Scanner;

public class zad4 {

    public static BigInteger szachownica(int n)
    {
        BigInteger rozmiar=BigInteger.valueOf(n*n);
        BigInteger wynik=BigInteger.valueOf(2);
        wynik=wynik.pow(n*n).subtract(BigInteger.valueOf(1));
        return wynik;
    }

    public static void main(String[] args) {
        Scanner in=new Scanner(System.in);
        System.out.println("Podaj rozmiar szachownicy: ");
        int n=in.nextInt();

        System.out.println("Ilość ziaren, które możemy na niej ułożyć: "+szachownica(n));
    }
}
