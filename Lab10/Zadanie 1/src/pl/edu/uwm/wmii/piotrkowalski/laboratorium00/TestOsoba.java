package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

import java.util.Collection;
import java.util.Collections;
import java.time.LocalDate;
import java.util.ArrayList;
import pl.imiajd.kowalski.Osoba;

public class TestOsoba {

    public static void main(String[] args) {
        ArrayList<Osoba> grupa=new ArrayList<Osoba>();

        grupa.add(new Osoba("Kowalski", LocalDate.parse("1999-01-29")));
        grupa.add(new Osoba("Nowak", LocalDate.parse("1999-01-29")));
        grupa.add(new Osoba("Kwiatkowska", LocalDate.parse("1992-01-23")));
        grupa.add(new Osoba("Szymańska", LocalDate.parse("1979-02-28")));
        grupa.add(new Osoba("Szymańska", LocalDate.parse("1993-01-19")));

        for (Osoba p:
             grupa) {
            System.out.println(p.toString());
        }

        System.out.println();
        Collections.sort(grupa);
        System.out.println();

        for (Osoba p:
                grupa) {
            System.out.println(p.toString());
        }
    }
}
