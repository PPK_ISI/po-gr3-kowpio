package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

import java.time.LocalDate;

public abstract class Osoba implements Cloneable, Comparable<Osoba>{
    public Osoba(String nazwisko, LocalDate dataUrodzenia){
        this.dataUrodzenia=dataUrodzenia;
        this.nazwisko=nazwisko;
    }

    public String toString(){
        return "Osoba ["+nazwisko+"] "+dataUrodzenia;
    }

    public boolean equals(Osoba T){
        if(this==T) return true;
        return false;
    }

    protected String nazwisko;
    protected LocalDate dataUrodzenia;

    public int compareTo(Osoba T) {
        if(this.nazwisko==T.nazwisko)
            if(this.dataUrodzenia==T.dataUrodzenia)
                return 1;
        return 0;
    }
}
