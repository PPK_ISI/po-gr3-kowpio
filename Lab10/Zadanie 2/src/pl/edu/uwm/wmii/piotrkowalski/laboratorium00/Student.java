package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

import java.time.LocalDate;

public class Student extends Osoba implements Comparable<Osoba>, Cloneable{
    public Student(String nazwisko, LocalDate dataUrodzenia, double sredniaOcen){
        super(nazwisko, dataUrodzenia);
        this.sredniaOcen=sredniaOcen;
    }

    public String toString(){
        return "Osoba ["+nazwisko+"] "+dataUrodzenia+" "+sredniaOcen;
    }

    public int compareTo(Student T) {
        if(this.nazwisko==T.nazwisko)
            if(this.dataUrodzenia==T.dataUrodzenia)
                if(this.sredniaOcen==T.sredniaOcen)
                    return 1;
        return 0;
    }

    private double sredniaOcen;
}
