package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

public class TestStudent {

    public static void main(String[] args) {
        ArrayList<Student> grupa=new ArrayList<Student>();

        grupa.add(new Student("Kowalski", LocalDate.parse("1999-01-29"), 3.7));
        grupa.add(new Student("Nowak", LocalDate.parse("1999-01-29"), 3.9));
        grupa.add(new Student("Kwiatkowska", LocalDate.parse("1992-01-23"), 3.8));
        grupa.add(new Student("Szymańska", LocalDate.parse("1979-02-28"), 4.2));
        grupa.add(new Student("Szymańska", LocalDate.parse("1993-01-19"), 4.1));

        for (Osoba p:
                grupa) {
            System.out.println(p.toString());
        }

        System.out.println();
        Collections.sort(grupa);
        System.out.println();

        for (Osoba p:
                grupa) {
            System.out.println(p.toString());
        }
    }
}
