package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

import pl.imiajd.kowalski.BetterRectangle;

import java.awt.*;

public class TestBetterRectangle {

    public static void main(String[] args) {

        BetterRectangle prostokat=new BetterRectangle(5, 6, 5, 6);
        System.out.println("Pole prostokata: "+prostokat.getArea());
        System.out.println("Obwód prostokata: "+prostokat.getPerimeter());

    }

}
