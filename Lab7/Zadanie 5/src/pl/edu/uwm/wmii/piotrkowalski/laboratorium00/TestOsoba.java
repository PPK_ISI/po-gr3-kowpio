package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

import pl.imiajd.kowalski.Nauczyciel;
import pl.imiajd.kowalski.Osoba;
import pl.imiajd.kowalski.Student;


public class TestOsoba {

    public static void main(String[] args) {
        Osoba osoba=new Osoba("Kowalski", "Piotr");
        Student student=new Student("Nowak", "Jan", "informatyka");
        Nauczyciel nauczyciel=new Nauczyciel("Kwiatkowska", "Anna", 3000);
        System.out.println(osoba.toString());
        System.out.println(student.toString());
        System.out.println(nauczyciel.toString());
        System.out.println("\nWywołania metod get: \n");
        System.out.print(osoba.getNazwisko()+" "+osoba.getImie()+"\n");
        System.out.print(student.getNazwisko()+" "+student.getImie()+" "+student.getKierunek()+"\n");
        System.out.print(nauczyciel.getNazwisko()+" "+nauczyciel.getImie()+" "+nauczyciel.getPensja()+"\n");
    }
}
