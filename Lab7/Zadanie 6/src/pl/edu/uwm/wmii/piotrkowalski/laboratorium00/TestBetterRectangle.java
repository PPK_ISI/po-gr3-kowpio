package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

import java.awt.*;

public class TestBetterRectangle {

    public static void main(String[] args) {

        BetterRectangle prostokat=new BetterRectangle(5, 6, 5, 6);
        System.out.println("Pole prostokata: "+prostokat.getArea());
        System.out.println("Obwód prostokata: "+prostokat.getPerimeter());

    }

    static class BetterRectangle extends Rectangle {

        public BetterRectangle(int x, int y, int width, int height) {
            super();
            setLocation(x,y);
            setSize(width, height);
        }

        public int getPerimeter(){
            return (2*width+2*height);
        }

        public int getArea(){
            return width*height;
        }

    }

}
