package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

public class TestOsoba {

    public static void main(String[] args) {
        Osoba osoba=new Osoba("Kowalski", "Piotr");
        Student student=new Student("Nowak", "Jan", "informatyka");
        Nauczyciel nauczyciel=new Nauczyciel("Kwiatkowska", "Anna", 3000);
        System.out.println(osoba.toString());
        System.out.println(student.toString());
        System.out.println(nauczyciel.toString());
        System.out.println("\nWywołania metod get: \n");
        System.out.print(osoba.getNazwisko()+" "+osoba.getImie()+"\n");
        System.out.print(student.getNazwisko()+" "+student.getImie()+" "+student.getKierunek()+"\n");
        System.out.print(nauczyciel.getNazwisko()+" "+nauczyciel.getImie()+" "+nauczyciel.getPensja()+"\n");
    }


    static class Osoba{

        private String nazwisko;
        private String imie;

        public Osoba(String nazwisko, String imie){
            this.nazwisko=nazwisko;
            this.imie=imie;
        }

        public Osoba(){
        }

        public String toString(){
            String wynik="";
            wynik=imie+" "+nazwisko;
            return wynik;
        }

        public String getImie(){
            return this.imie;
        }

        public String getNazwisko(){
            return this.nazwisko;
        }

    }


    static class Student extends Osoba{

        private String nazwisko;
        private String imie;
        private String kierunek;

        public Student(String nazwisko, String imie, String kierunek){
            this.nazwisko=nazwisko;
            this.imie=imie;
            this.kierunek=kierunek;
        }

        public String toString(){
            String wynik="";
            wynik=imie+" "+nazwisko+" "+kierunek;
            return wynik;
        }

        public String getImie(){
            return this.imie;
        }

        public String getNazwisko(){
            return this.nazwisko;
        }

        public String getKierunek(){
            return kierunek;
        }

    }


    static class Nauczyciel extends Osoba{

        private double pensja;
        private String nazwisko;
        private String imie;

        public Nauczyciel(String nazwisko, String imie, double pensja){
            this.nazwisko=nazwisko;
            this.imie=imie;
            this.pensja=pensja;
        }

        public String toString(){
            String wynik="";
            wynik=imie+" "+nazwisko+" "+pensja;
            return wynik;
        }

        public String getImie(){
            return this.imie;
        }

        public String getNazwisko(){
            return this.nazwisko;
        }

        public double getPensja(){
            return pensja;
        }

    }

}
