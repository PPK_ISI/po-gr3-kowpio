package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

import pl.imiajd.kowalski.NazwanyPunkt;
import pl.imiajd.kowalski.Punkt;

public class TestNazwanyPunkt
{
    public static void main(String[] args)
    {
        NazwanyPunkt a = new NazwanyPunkt(3, 5, "port");
        a.show();

        Punkt b = new Punkt(3, 5);
        b.show();

        Punkt c = new NazwanyPunkt(3, 6, "tawerna");
        c.show();

        //a = b;               //  --- powoduje błąd kompilacji  (dlaczego ?)
        //a = (NazwanyPunkt) b;  //   --- powoduje błąd wykonania   (dlaczego ?)
        a = (NazwanyPunkt) c;
        a.show();
    }
}



