package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

import java.util.Scanner;

public class Main {

    public static int[] wyniki(int[] tab, int n){
        int mniejsze=0;
        int wieksze=0;
        int rowne=0;
        for(int i=0; i<n; i++){
            if(tab[i]>5)
                wieksze++;
            else if(tab[i]<-5)
                mniejsze++;
            else if(tab[i]==-5)
                rowne++;
        }
        int[] wynik=new int[3];
        wynik[0]=mniejsze;
        wynik[1]=wieksze;
        wynik[2]=rowne;
        return wynik;
    }

    public static void main(String[] args) {
        Scanner input=new Scanner(System.in);
        System.out.println("Podaj ilosc liczb: ");
        int n= input.nextInt();
        int[] tab=new int[n];
        for(int i=0; i<n; i++){
            System.out.println("Podaj "+(i+1)+" liczbe: ");
            tab[i]= input.nextInt();
        }
        int[] x=wyniki(tab,n);
        System.out.println("Ilosc liczb wiekszych od 5: "+x[1]);
        System.out.println("Ilosc liczb mniejszych od -5: "+x[0]);
        System.out.println("Ilosc liczb rownych -5: "+x[2]);
    }
}
