package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

import java.util.Scanner;

public class Main {

    public static String delete(String str, char c){
        int n=str.length();
        int licznik=0;
        char[] wynik=new char[n];
        for(int i=0; i<n; i++){
            if(licznik!=0){
                if(wynik[i]==c)
                    wynik[i]=' ';
            }
            else licznik++;
        }
        String x=wynik.toString();
        return x;
    }

    public static void main(String[] args) {
        Scanner input=new Scanner(System.in);
        System.out.println("Podaj napis: ");
        String str=input.nextLine();
        System.out.println("Podaj znak do wyciecia: ");
        String c1=input.nextLine();
        char c2=c1.charAt(0);
        System.out.println("Napis po wycieciu znaku: "+delete(str, c2));
    }
}
