package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

import javax.sound.midi.Soundbank;
import java.sql.SQLOutput;
import java.util.*;


public class Main {

    public static void main(String[] args) {
        Map<Student, String> students = new TreeMap<>();

        students.put(new Student("Carl", "James", 1), "db+");students.put(new Student("Joe", "Smith", 2), "db");students.put(new Student("Susan", "Collins", 3), "bdb");

        System.out.println("Aktualny stan listy studentów:");show_set(students);System.out.println();
        System.out.println();

        String option="";
        Scanner option_input = new Scanner(System.in);
        Scanner new_name = new Scanner(System.in);
        Scanner new_last_name = new Scanner(System.in);
        Scanner new_id = new Scanner(System.in);
        Scanner new_degree = new Scanner(System.in);
        String name = ""; String degree = ""; String last_name = ""; int id = 0;
        boolean x=true;

        while(x){
            System.out.println("Podaj opcję:\n-dodaj/edytuj\n-usun\n-zakoncz");
            option = option_input.nextLine();
            switch (option){
                case "dodaj/edytuj":
                    System.out.println("Podaj imię studenta:");
                    name = new_name.nextLine();
                    System.out.println("Podaj nazwisko studenta:");
                    last_name = new_last_name.nextLine();
                    System.out.println("Podaj identyfikator studenta:");
                    id = new_id.nextInt();
                    System.out.println("Podaj ocenę studenta:");
                    degree = new_degree.nextLine();
                    students.put(new Student(name, last_name, id), degree);
                    break;

                case "usun":
                    System.out.println("Podaj nazwisko studenta:");
                    name = new_name.nextLine();
                    students.remove(Student.getId());
                    break;

                case "zakoncz":
                    x=false;

                default:
                    System.out.println("Podano nieprawidłowe polecenie");
            }
            System.out.println("Aktualny stan listy studentów:");show_set(students);System.out.println();
        }
    }

    static void show_set(Map<Student, String> map){
        TreeMap<Student, String> sorted = new TreeMap<>();
        sorted.putAll(map);
        for (Map.Entry<Student, String> entry: sorted.entrySet()){
            System.out.println(entry.getKey()+": "+entry.getValue());
        }
    }

}

class Student{

    public Student(String imie, String nazwisko, int id){
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.id = id;
    }

    public static int getId(){
        return id;
    }

    private String imie;
    private String nazwisko;
    private static int id;
    
}
