package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

import javax.sound.midi.Soundbank;
import java.sql.SQLOutput;
import java.util.*;


public class Main {

    public static void main(String[] args) {
	    Map<String, String> students = new TreeMap<>();
	    students.put("Carl", "db+");students.put("Joe", "db");students.put("Susan", "bdb");
        System.out.println("Aktualny stan listy studentów:");show_set(students);System.out.println();
        System.out.println();
	    String option="";
	    Scanner option_input = new Scanner(System.in);
	    Scanner new_name = new Scanner(System.in);
	    Scanner new_degree = new Scanner(System.in);
	    String name = ""; String degree = "";
	    boolean x=true;
	    while(x){
            System.out.println("Podaj opcję:\n-dodaj/edytuj\n-usun\n-zakoncz");
            option = option_input.nextLine();
            switch (option){
                case "dodaj/edytuj":
                    System.out.println("Podaj nazwisko studenta:");
                    name = new_name.nextLine();
                    System.out.println("Podaj ocenę studenta:");
                    degree = new_degree.nextLine();
                    students.put(name, degree);
                    break;

                case "usun":
                    System.out.println("Podaj nazwisko studenta:");
                    name = new_name.nextLine();
                    students.remove(name);
                    break;

                case "zakoncz":
                    x=false;

                default:
                    System.out.println("Podano nieprawidłowe polecenie");
            }
            System.out.println("Aktualny stan listy studentów:");show_set(students);System.out.println();
        }
    }

    static void show_set(Map<String, String> map){
        TreeMap<String, String> sorted = new TreeMap<>();
        sorted.putAll(map);
        for (Map.Entry<String, String> entry: sorted.entrySet()){
            System.out.println(entry.getKey()+": "+entry.getValue());
        }
    }

}
