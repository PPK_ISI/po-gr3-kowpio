package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

import java.util.PriorityQueue;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner priority_in = new Scanner(System.in);
        Scanner desc_in = new Scanner(System.in);
        Scanner input = new Scanner(System.in);
        String desc;
        int priority;
        int loop=1;

        String x="";

        PriorityQueue<Task> p_queue = new PriorityQueue<Task>();

        System.out.println("Dostępne polecenia:\n-dodaj\n-następne\n-zakończ\n");
        while(loop==1) {
            System.out.println("Podaj polecenie:");
            x = input.nextLine();
            switch (x) {

                case "dodaj":
                    System.out.println("Podaj priorytet zadania");
                    priority = priority_in.nextInt();
                    System.out.println("Podaj opis zadania");
                    desc = desc_in.nextLine();
                    Task task = new Task(priority, desc);
                    p_queue.offer(task);
                    break;

                case "następne":
                    if(!p_queue.isEmpty())
                        System.out.println("Usunięto zadanie: "+p_queue.poll().getDescription());
                    break;

                case "zakończ":
                    loop=0;
                    break;

                default:
                    System.out.println("Niepoprawne polecenie!");
                    break;

            }
            System.out.println("Aktualny szczyt kolejki: "+p_queue.peek().getDescription());
        }

    }

    public static class Task implements Comparable<Task>{

        private int priority = 0;
        private String description = null;

        public Task(int priority, String description){
            this.priority = priority;
            this.description = description;
        }

        public String getDescription() {
            return description;
        }

        public int getPriority(){
            return priority;
        }

        public int compareTo(Task o) {
            return 0;
        }
    }

}
