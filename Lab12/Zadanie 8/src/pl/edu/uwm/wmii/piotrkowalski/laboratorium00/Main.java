package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

public class Main {

    public static <T extends Iterable<T>> void print(T[] t){
        for (T e:t) {
            System.out.print(e+", ");;
        }
    }

    public static void main(String[] args) {
	    String[] t1=new String[4];
	    t1[0]="123";t1[1]="123";t1[2]="123";t1[3]="123";
	    print(t1);
    }
}
