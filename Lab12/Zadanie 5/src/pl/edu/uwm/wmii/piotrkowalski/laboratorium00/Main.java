package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

import java.util.Stack;

public class Main {

    public static String reverse(String zdanie){
        Stack<String> stos=new Stack<String>();
        String container="";
        for(int i=0; i<zdanie.length(); i++){
            if(zdanie.charAt(i)==' '){
                stos.add(container);
                container="";
            }
            else
                container= container+zdanie.charAt(i);
        }
        stos.add(container);
        String wynik="";
        while(!stos.isEmpty()){
            container=stos.peek();
            wynik+=container+" ";
            stos.pop();
        }
        wynik=wynik.substring(0, 1).toUpperCase()+wynik.substring(1)+".";
        return wynik;
    }

    public static void main(String[] args) {

        String zdanie="Ala ma kota. Jej kot lubi myszy.";
        String[] zdania = zdanie.split("\\.");
        System.out.println(reverse(zdania[0])+" "+reverse(zdania[1]));
    }
}
