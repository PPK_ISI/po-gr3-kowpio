package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

public class Main {

    public static void main(String[] args) {
        int n=60;
        Boolean[] t=new Boolean[n+1];

        for(int i=0; i<=n; i++)
            t[i]=true;

        for(int i=2; i<= Math.sqrt(n); i++)
            if(t[i]==true)
                for(int j=2*i; j<=n; j+=i)
                    t[j]=false;

        System.out.println("liczby pierwsze z zakresu [2; "+n+"] :");
        for(int i=2; i<n; i++)
            if(t[i]==true)
                System.out.print(i+",");

    }

}
