package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

import java.util.LinkedList;

public class RedukujTest {

    public static void redukuj(LinkedList<String> pracownicy, int n){
        for(int i=n; i< pracownicy.size(); i=i+n)
            pracownicy.remove(i);
    }


    public static void main(String[] args) {
        LinkedList<String> pracownicy = new LinkedList<String>();
        String pracownik="";
        for(int i=0; i<10; i++){
            pracownik="pracownik"+i;
            pracownicy.add(pracownik);
        }
        for(String p: pracownicy)
            System.out.println(p);

        redukuj(pracownicy, 2);
        System.out.println();

        for(String p: pracownicy)
            System.out.println(p);
    }
}
