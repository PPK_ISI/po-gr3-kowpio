package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

import java.util.LinkedList;

public class OdwrocTest {

    public static void odwroc(LinkedList<String> pracownicy){
        int l=pracownicy.size();
        String help;
        for(int i=0; i<l/2; i++){
            help=pracownicy.get(i);
            pracownicy.set(i, pracownicy.get(l-i-1));
            pracownicy.set(l-i-1, help);
        }

    }


    public static void main(String[] args) {
        LinkedList<String> pracownicy = new LinkedList<String>();
        String pracownik="";
        for(int i=0; i<10; i++){
            pracownik="pracownik"+i;
            pracownicy.add(pracownik);
        }
        for(String p: pracownicy)
            System.out.println(p);

        odwroc(pracownicy);
        System.out.println();

        for(String p: pracownicy)
            System.out.println(p);
    }
}
