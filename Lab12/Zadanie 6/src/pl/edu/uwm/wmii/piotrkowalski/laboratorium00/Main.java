package pl.edu.uwm.wmii.piotrkowalski.laboratorium00;

import java.util.Stack;

public class Main {

    public static void main(String[] args) {
        Integer liczba=2015;
        if(liczba<0)
            System.out.println("niepoprawna liczba");
        else {
            Stack<Integer> stos = new Stack<Integer>();
            while (liczba > 0) {
                stos.push(liczba % 10);
                liczba = liczba / 10;
            }
            while (!stos.isEmpty()) {
                System.out.print(stos.peek() + " ");
                stos.pop();
            }
        }
    }
}
